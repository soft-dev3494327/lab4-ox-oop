/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nalinthip.lab4oxoop;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Game {

    private Player player1, player2;
    private Board board;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printBoard();
            printTurn();
            inputRowCol();
            if (board.checkWin()) {
                printBoard();
                printWinner();
                printPlayers();
                isFinish = true;
            }
            if (board.checkDraw()) {
                printBoard();
                printDraw();
                printPlayers();
                isFinish = true;
            }
            board.switchPlayer();
        }
        printContinue();
    }

    private void printWelcome() {
        System.out.println(" ______________________________");
        System.out.println("|                              |");
        System.out.println("|      Welcome to XO Game      |");
        System.out.println("|______________________________|");
    }

    private void printBoard() {
        char[][] b = board.getBoard();
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                System.out.print(b[i][j] + "|");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println("player " + board.getCurrentPlayer().getSymbol() + " turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Please input number[1-3] in row/col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        board.setRowCol(row, col);

    }

    private void newGame() {
        board = new Board(player1, player2);
    }

    private void printWinner() {
        System.out.println(" ____________________________________ ");
        System.out.println("|                                    |");
        System.out.println("|  Congratulations! Player " + board.getCurrentPlayer().getSymbol() + " Win!!! |");
        System.out.println("|____________________________________|");
    }

    private void printDraw() {
        System.out.println(" ____________________");
        System.out.println("|                    |");
        System.out.println("|        Draw!       |");
        System.out.println("|____________________|");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);

    }

    private void printContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Would you like to play again? (y/n) : ");
        String newgame = sc.next();
        if (newgame.equals("y")) {
            play();
        } else {
            System.out.println(" ____________________");
            System.out.println("|                    |");
            System.out.println("|     Exit Game      |");
            System.out.println("|      ByeBye!!      |");
            System.out.println("|____________________|");
            System.exit(0);
        }
    }
}
