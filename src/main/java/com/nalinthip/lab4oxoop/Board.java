/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nalinthip.lab4oxoop;

/**
 *
 * @author ACER
 */
public class Board {

    private char[][] board = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    private Player player1, player2, currentPlayer;
    private int row, col;
    private int count;

    public Board(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;

    }

    public char[][] getBoard() {
        return board;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (board[row - 1][col - 1] == '_') {
            board[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            saveWin();
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        if (count == 9) {
            player1.draw();
            player2.draw();
            return true;
        }
        return false;
    }

    private boolean checkRow() {
        return board[row - 1][0] != '_' && board[row - 1][0] == board[row - 1][1] && board[row - 1][0] == board[row - 1][2];
    }

    private boolean checkCol() {
        return board[0][col - 1] != '_' && board[0][col - 1] == board[1][col - 1] && board[0][col - 1] == board[2][col - 1];
    }

    private boolean checkX1() {
        return board[0][0] == currentPlayer.getSymbol() && board[1][1] == currentPlayer.getSymbol() && board[2][2] == currentPlayer.getSymbol();
    }

    private boolean checkX2() {
        return board[0][2] == currentPlayer.getSymbol() && board[1][1] == currentPlayer.getSymbol() && board[2][0] == currentPlayer.getSymbol();
    }

    private void saveWin() {
        if (currentPlayer == player1) {
            player1.win();
            player2.lose();
        } else {
            player1.lose();
            player2.win();
        }
    }

    void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

}
